# Installation
First download the file for your distribution, here for example download "fedora_post_installation.sh".
I use `wget` in the example below, feel free to use your own method to download.
```bash
wget https://gitlab.com/bitbase/Linux_Post_Installation_Setup/blob/master/fedora_post_installation.sh
```

1. Switch to the root user
```bash
su
```

2. Make the script file executable
```bash
chmod +x "fedora_post_installation.sh"
```

3. Run the script as the root user
```bash
./fedora_post_installation.sh
```
