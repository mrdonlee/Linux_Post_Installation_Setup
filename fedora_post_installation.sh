#!/usr/bin/bash

echo "Enter a hostname:"
read hostname

# 01. Set Hostname
hostnamectl set-hostname "$hostname"
systemctl restart systemd-hostnamed.service

# 02. Update the system
dnf update -y

# 03. Enable RPM Fusion repositories
dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# 04. Enable Delta RPM and fastest mirrors
echo "fastestmirror=true" >> /etc/dnf/dnf.conf
echo "deltarpm=true" >> /etc/dnf/dnf.conf

# 05. Install multimedia codecs and frameworks
dnf install -y ffmpeg gstreamer gstreamer-plugins-* gstreamer1-plugins-*

# 06. Install VLC media player
dnf install -y vlc

# 07. Install YouTube downloader
dnf install -y youtube-dl

# 08. Install wine
dnf install -y wine

# 09. Install compression and archiving tools
dnf install -y unzip unrar

# 10. Install JAVA web plugins
dnf install -y java-openjdk icedtea-web

# 11. Install Bleachbit
dnf install -y bleachbit

# 12. Install steam
dnf install -y steam

# 13. Install Dropbox
dnf install -y dropbox

# 14. Install Thunderbird email client
dnf install -y thunderbird

# 15. Install Graphic tools
dnf install -y gimp inkscape

# 16. Install qBittorrent
dnf install -y qbittorrent

# 17. Install VirtualBox
dnf config-manager -y --add-repo https://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
dnf install -y VirtualBox

# 18. Install KeePass2 password safe
dnf install -y keepass

# 19. Install Lutris gaming platform
dnf config-manager -y --add-repo https://download.opensuse.org/repositories/home:strycore/Fedora_$(rpm -E %fedora)/home:strycore.repo
dnf install -y lutris

# 20. Install KDE Plasma desktop
dnf install -y @kde-desktop
