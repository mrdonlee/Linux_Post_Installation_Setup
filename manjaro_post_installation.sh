#!/usr/bin/bash


# 01. Update the system
pacman -Syu

# 02. Install YouTube downloader
pacman -S youtube-dl

# 03. Install wine
pacman -S wine wine-mono winetricks

# 04. Install Adobe Flash Player plugin
pacman -S flashplugin

# 05. Install Aria2 download utility
pacman -S aria2

# 06. Install Graphic tools
pacman -S inkscape

# 07. Install qBittorrent
pacman -S qbittorrent

# 08. Install VirtualBox
pacman -S virtualbox

# 09. Install KeePass2 password safe
pacman -S keepass

# 10. Install system monitor software
pacman -S conky

# 11. Install veracrypt
pacman -S veracrypt

# 12. Install Audacity
pacman -S audacity

# 13. Install Kdenlive
pacman -S 

# 14. Install Mixxx
pacman -S mixxx

# 15. Install Open Broadcaster 
pacman -S 

# 16. Install PlayOnLinux
pacman -S

# 17. Install qTox
pacman -S

# 18. Install Syncthing
pacman -S

# 19. Install Tor Browser
pacman -S

# 20. Install Telegram desktop
pacman -S
